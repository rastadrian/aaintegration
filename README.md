# Android Annotations Integration Demo

This project shows a basic implementation of Android Annotations. It is the demo project for [Monkey's Code Android Annotations in Android Studio tutorial](https://rastadrian.com/index.php/2016/10/31/integrate-android-annotations-in-android-studio/).
